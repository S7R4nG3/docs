# Doc Home

Welcome to my documentation site. Hopefully this will help me keep all my data straightforward in case I need to rebuild something.

## About

I'm hoping to use this site as a repository of info for my entire homebuild and setup. Ideally, I could use this doc site to reference in the event of a system outage of some sort to identify the configuration of my services in the event of a rebuild.

So far, I really like this setup though!

## Things to Do

* Document network connections
* Find a good IPAM provider
* Get VMUG license setup
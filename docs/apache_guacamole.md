# Guac Config

The following information is the current configuration of Apache Guacamole v0.9.14.

## Virtual Machine Config

An Ubuntu 18.04 server VM was created with the following config

* 2 vCPUs
* 8 GB RAM
* 16 GB drive attached

Ubuntu was then installed on the VM and immediately updated/upgraded with latest patches.

Networking was configured using /etc/netplan to apply a static IP and my DNS server.

SSH keys were then migrated into the box for my laptop and desktop to allow for easier access. These keys are backed up in the cloud for reference.

## Setup

Guacamole has a fair number of dependencies, and failure to include these dependencies can cause a TON of problems (believe me, I tried).

Thus to allow for easier setup, I was able to locate some bash scripts that automate the install and its dependencies by GitHub user - MysticRyuujin.

[Link to his GitHub Repo](https://github.com/MysticRyuujin/guac-install/blob/master/guac-install.sh)

Local copies:

* [install](./local_scripts/guac-install.txt)

* [upgrade](./local_scripts/guac-upgrade.txt)

* [server install](./local_scripts/guac-install-server.txt)

* [docker install](./local_scripts/docker-install.txt)

The install is fairly straightforward and requires the setup of a Postgres database to hold machine and user information.

Follow the prompts through and configure it as close to the defaults as necessary. I don't think I made any added configuration changes.

Once the install completes, the server should be accessible on port 8080 of localhost and use guacadmin/guacadmin as the login.

Once logged in, setup your personal admin account and shutdown the login and use of the guacadmin user.

Be sure to create the DNS entries for the host to allow for easier access within the network.

The guac server should now be fully functional and only require the addition of clients.

## Remote Access

If remote access is wanted, outside of using a VPN its necessary to setup port forwarding on the router.

Port forward a random port on the router to port 8080 to the IP address used in the VM config. Obviously this is a terrible idea since it leaves the system exposed to the internet, and anyone that gets access to your credentials will be able to access any machine you have.

**DONT USE THE ABOVE! -- SETUP A VPN YOU IDIOT!**
# Media Server

The primary media server is currently running as a single Ubuntu VM on the F15 host. This VM has been allocated the majority of the host's HDD space since it was only provided with a 500gb drive.

This should be fully upgraded in the future with the usage of the storage server by running the entire configuration via jails.

## VM Configuration

The virtual machine was configured as follows:

* 2 vCPUs
* 6 GB RAM
* 150 GB disk space

Ubuntu was installed on the VM and immediately updated/upgraded to include the latest patches.

A static IP was set using /etc/netplan and the local DNS server added for ease of access.

Laptop and Desktop SSH keys were then added to the machine for ease of accessing.

## Docker Setup

With Ubuntu up and running, Docker was added to the machine as per their main instructions on their website.

I have compiled all the necessary commands into a shell script that automatically installs both Docker as well as Portainer (for UI management).

[Install script](./local_scripts/docker-installer.txt)

This script should be run as root on the new box and will immediately start Portainer which can be accessed on the localhost on port 9000.

You will immediately be prompted to setup admin credentials in Portainer, and you should immediately enter and rename the Portainer container to "port" to start/stop easier. If this is not renamed you will need to SSH into the box and use "docker ps" to identify the randomized name for the container.

## Containers

The current list of containers on this box are as follows:

* SABnzbd - Usenet Downloader
* Sonarr - TV Shows
* Radarr - Movies
* Transmission - Download Client
* Portainer - UI Management
* Plex - Media Server

Each of these containers has been setup with its own _service_\_config/ volume directory that can be located at /var/lib/docker/volumes.

**Important Note** The permissions on the /docker/ and docker/volumes directories is very important to allow each container to be able to access shared directories. It is important to CHOWN these directories to the appropriate user account for proper access.


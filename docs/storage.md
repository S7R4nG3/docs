# Storage

This page will be a work in progress for a little bit until I get the hardware fully situated on the media server.

## Setup

The NAS box is configured with the following hardware:

* SuperMicro 933 Chassis
	* 14 3.5 bays
	* 4x 92mm SanAce 3pin intake fans
	* 2x 80mm SanAce 3pin exhaust fans
* SuperMicro X8DTH-iF motherboard
* 2x Intel Xeon 5660 CPUs (6 core)
* 12GB DDR3 1333mHz ECC (6x2GB sticks)
* 6x 4TB Western Digital RE HDDs
* 1x LSI 9211-8i HBA (in IT Mode) _This will need a second to allow for usage of all 14 bays_
* 1x SanDisk 16gb thumb drive (internal) _For the operating system_

FreeNAS was installed to the internal 16gb thumb drive for quicker boot and to utilize as much space on the 3.5 drives as possible.

## Pool & Dataset Setup

Currently, all 6 drives are allocated into a single RAIDZ3 pool (RAID6) to allow for 2 disk fault tolerance.

This pool was then divided into 3 primary datasets:

* /media/ - Used to house all media to reclaim space on my Desktop
* /windows-share/ - Used as a general storage location for important documents
* /linux-share/ - Used as a general storage location for linux docs

A "share-group" user group was created to hold the permissions on accessing both the Windows and Linux shares.

A user account was then created with this group as an additional group to allow that user access to the shares.

Finally, the Windows and Linux shares were created, granting access to the "share-group" user group to allow login access for write operations.
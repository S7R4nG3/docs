# DNS Services

## VM Configuration

A Windows Server 2016 VM was created with the following config:

* 1 vCPU
* 4 GB RAM
* 40 GB drive space

This drive size was much larger than necessary for this VM, since at the time I was looking into using it for other tasks. If rebuilding, the VM can be created with a minimum disk sizing.

Windows Server 2016 Standard was installed on an evaluation license using a standard install WITH a GUI. The GUI is not necessary and instead a Server Core installation could be used with the help of PowerShell scripts to automate the rebuild.

## Role Setup

The server was configured with DNS Manager roles and RSAT tools to allow remote configuration if necessary.

## Naming Conventions

All ESXi hosts have IP address allocations of even 10s with their services running on the subsequent 9 IP addresses.

As of now I do not have any ESXi hosts running more than 9 VMs, however when this is expanded the DNS conventions will be updated.